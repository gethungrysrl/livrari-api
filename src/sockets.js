const clients = {};

const onUserJoined = (userId, socket) => {
  clients[userId] = socket;
};

const onUserLeft = userId => {
  delete clients[userId];
};

exports.getSocket = userId => clients[userId];

exports.startSockets = io => {
  io.on('connection', socket => {
    socket.on('userJoined', userId => {
      return onUserJoined(userId, socket);
    });

    socket.on('userLeft', userId => onUserLeft(userId, socket));
  });

  io.on('error', err => {
    console.log('SOCKET ERROR');
    console.log(err.stack);
  });
};
