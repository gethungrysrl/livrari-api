const cron = require('node-cron');

const { eventEmitter } = require('./services');

const { sentPushNotification } = require('./services/sendNotifications');
const { eventTypes: accountEventTypes } = require('./constants');
const {
  sendPasswordResetEmail,
  sendSignUpConfirmationEmail,
} = require('./listeners');

const bootstrap = () => {
  eventEmitter.on(accountEventTypes.SIGN_UP, sendSignUpConfirmationEmail);

  eventEmitter.on(
    accountEventTypes.PASSWORD_RESET_TOKEN_CREATED,
    sendPasswordResetEmail,
  );

  // Run 'expire' task at startup and set up 'expire' cron job
  // Runs at every xx:01
  // cron.schedule('5,10,15,20,25,30,35,40,45,50,55 * * * * *', async () => {});
  // cron.schedule("1 * * * *", async () => {});
};

module.exports = bootstrap;
