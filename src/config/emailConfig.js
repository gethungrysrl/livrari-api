exports.from = "Achizitii Proiecte <support@achizitii-proiecte.ro>";

exports.transporter = {
  host: process.env.EMAIL_TRANSPORTER_HOST,
  port: process.env.EMAIL_TRANSPORTER_PORT,
  secure: process.env.EMAIL_TRANSPORTER_SECURE,
  auth: {
    user: process.env.EMAIL_TRANSPORTER_AUTH_USER,
    pass: process.env.EMAIL_TRANSPORTER_AUTH_PASSWORD
  }
};

//de modificat
