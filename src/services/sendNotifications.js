const { Expo } = require('expo-server-sdk');
const { map } = require('lodash');

const { UserToken } = require('../models');

const sockets = require('../sockets');

const expo = new Expo();

const sendFromSocket = (emit, { userId, ...args }) => {
  if (sockets.getSocket(userId)) {
    try {
      sockets.getSocket(userId).emit(emit, { ...args });
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log('error');
    }
  }
};

const sentPushNotification = async (userId, { title, body }) => {
  const pushTokens = await UserToken.find({
    userId,
    type: 'pushToken',
  });
  console.log(pushTokens);
  try {
    const chunks = expo.chunkPushNotifications(
      map(pushTokens, ({ token }) => ({
        to: token,
        // icon: `${appConfig.appBaseUrl}/assets/notification/logo`,
        sound: 'default',
        title,
        body,
        priority: 'high',
        channelId: 'chat-messages',
      })),
    );
    // eslint-disable-next-line no-restricted-syntax
    for (const chunk of chunks) {
      console.log(chunk);
      try {
        await expo.sendPushNotificationsAsync(chunk);
      } catch (e) {
        console.log('error');
      }
    }
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
};

const sendNotifications = async (emit, notifications) => {
  notifications.map(({ userId, title, body, ...args }) => {
    sendFromSocket(emit, { userId, ...args });
    sentPushNotification(userId, { title, body });
  });
};

module.exports = { sendNotifications, sendFromSocket, sentPushNotification };
