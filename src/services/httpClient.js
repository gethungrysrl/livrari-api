const { isPlainObject } = require('lodash');
const humps = require('humps');
const axios = require('axios');

const transformResponse = data =>
  isPlainObject(data) ? humps.camelizeKeys(data) : data;

const httpClient = axios.create({
  transformResponse: axios.defaults.transformResponse.concat(transformResponse),
});

module.exports = httpClient;
