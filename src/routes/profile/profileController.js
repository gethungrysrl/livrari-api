const { profileRepository, orderRepository } = require('../../repositories');

exports.getProfile = async (req, res) => {
  const profile = await profileRepository.findByUserId(req.userId);
  if (!profile) {
    return res.status(404).json({ errors: { profile: 'Not found' } });
  }
  res.json(profile);
};

exports.findOneByUserIdAndUpdate = async (req, res) => {
  const { userId } = req;
  const { profile } = req.body;
  const newProfile = await profileRepository.findOneByUserIdAndUpdate(
    userId,
    profile,
  );
  res.json(newProfile);
};

exports.isWorking = async (req, res) => {
  const profile = await profileRepository.isWorkingHandler(req.userId);
  if (profile.isWorking) {
    orderRepository.asignOrders(req.userId);
  }
  res.json(profile);
};
