const express = require('express');

const {
  authMiddleware,
  asyncMiddleware,
  validatorMiddleware,
  roleMiddleware,
} = require('../../middlewares');

const profileController = require('./profileController');
const { userRoles } = require('../../constants');

const router = express.Router();

router.get(
  '/profile/get',
  validatorMiddleware,
  authMiddleware,
  asyncMiddleware(profileController.getProfile),
);

router.post(
  '/profile/edit',
  authMiddleware,
  validatorMiddleware,
  asyncMiddleware(profileController.findOneByUserIdAndUpdate),
);

router.put(
  '/profile/isWorking',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COURIER]),
  asyncMiddleware(profileController.isWorking),
);

module.exports = router;
