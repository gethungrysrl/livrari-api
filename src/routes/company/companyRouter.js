const express = require('express');

const {
  authMiddleware,
  asyncMiddleware,
  validatorMiddleware,
  roleMiddleware,
} = require('../../middlewares');

const { userRoles } = require('../../constants');

const companyController = require('./companyController');

const router = express.Router();

router.get(
  '/company/get',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY]),
  asyncMiddleware(companyController.findOneByUserId),
);

router.post(
  '/company/edit',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY]),
  asyncMiddleware(companyController.findOneByUserIdAndUpdate),
);

router.get(
  '/company/getActive',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.USER, userRoles.ADMIN]),
  asyncMiddleware(companyController.findActive),
);

module.exports = router;
