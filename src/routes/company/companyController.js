const { companyRepository } = require('../../repositories');

exports.findOneByUserId = async (req, res) => {
  const { userId } = req;
  let company = await companyRepository.findOneByUserId(userId);
  if (!company) {
    company = await companyRepository.create({ userId });
  }

  res.json(company);
};

exports.findOneByUserIdAndUpdate = async (req, res) => {
  const { userId } = req;
  const { company } = req.body;
  const newCompany = await companyRepository.findOneByUserIdAndUpdate(
    userId,
    company,
  );
  res.json(newCompany);
};

exports.findActive = async (req, res) => {
  const companys = await companyRepository.findActive();
  res.json(companys);
};
