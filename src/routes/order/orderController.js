/* eslint-disable no-use-before-define */
const { reduce, filter, sortBy } = require('lodash');
// const fetch = require('node-fetch');

const {
  orderRepository,
  companyRepository,
  userTokenRepository,
} = require('../../repositories');
const { sendNotifications } = require('../../services');
const { orderStatus } = require('../../constants');

exports.findByCompanyId = async (req, res) => {
  const { userId } = req;
  const orders = await orderRepository.findByCompanyId(userId);
  res.json(orders);
};

// const handlerDeliveringOrders = async (orders, courierLocation) => {
//   try {
//     const waypoints = map(
//       filter(orders, el => el.location),
//       el => `${el.location.latitude},${el.location.longitude}`,
//     ).join('|');
//     const response = await fetch(
//       `https://maps.googleapis.com/maps/api/directions/json?origin=${courierLocation}&destination=${courierLocation}&
//       waypoints=optimize:true|${waypoints}&key=${process.env.GOOGLE_DIRECTIONS_API_KEY}`,
//     );
//     const json = await response.json();
//     return json.routes[0].waypoint_order && json.routes[0].waypoint_order.length
//       ? map(json.routes[0].waypoint_order, index => orders[index])
//       : handleOrders(orders);
//   } catch (e) {
//     return handleOrders(orders);
//   }
// };

exports.findByCourierId = async (req, res) => {
  const { userId } = req;
  // const { location } = req.params;
  const orders = await orderRepository.findByCourierId(userId);
  const preparingOrders = filter(
    orders,
    order => order.status === orderStatus.PREPARING,
  );
  // const deliveringOrders = await handlerDeliveringOrders(
  const deliveringOrders = filter(
    orders,
    order =>
      order.status === orderStatus.DELIVERING ||
      order.status === orderStatus.WAITING,
  );
  //   location,
  // );
  const finishedOrders = filter(
    orders,
    order => order.status === orderStatus.FINISHED,
  );

  res.json({ preparingOrders, deliveringOrders, finishedOrders });
};

exports.findByUserId = async (req, res) => {
  const { userId } = req;
  const orders = await orderRepository.findByUserId(userId);
  res.json(orders);
};

exports.getDetails = async (req, res) => {
  const { orderId } = req.params;
  const details = await orderRepository.detailsOrder(orderId);
  res.json(details);
};

exports.nextStatus = async (req, res) => {
  const { orderId } = req.params;
  const order = await orderRepository.nextStatus(orderId);
  res.json(order);
};

const latitudes = [47552491, 47557978, 47517017, 47546473, 47573153];
const longitudes = [25914942, 25918339, 25863388, 25867140, 25879142];
const maxDistance = 35000;

const checkDistace = location => {
  const arithmeticLatitudes =
    reduce(latitudes, (total, el) => total + el, 0) / latitudes.length;
  const arithmeticLongitudes =
    reduce(longitudes, (total, el) => total + el, 0) / longitudes.length;

  const distance = Math.sqrt(
    (arithmeticLatitudes - location.latitude * 1000000) ** 2 +
      (arithmeticLongitudes - location.longitude * 1000000) ** 2,
  );

  return distance <= maxDistance;
};

exports.create = async (req, res) => {
  const order = req.body;
  if (!order.location || !checkDistace(order.location)) {
    res
      .status(422)
      .json({ message: 'Locatia nu se afla in raza noastra de actiune' });
    return;
  }
  const company = await companyRepository.findById(order.companyId);
  await orderRepository.create({
    ...order,
    userId: req.userId,
    companyId: company.userId,
    transportFee: company.transportFee,
    companyName: company.name,
    companyLocation: company.location.geometry.location,
    status: orderStatus.AWAIT,
  });
  const userPushToken = await userTokenRepository.findPushToken(company.userId);
  if (userPushToken) {
    sendNotifications.sendNotifications('newOrder', [
      {
        userId: company.userId,
        pushToken: userPushToken.token,
        title: 'Comanda noua',
        body: 'Intra acum sa vezi detaliile comenzii.',
      },
    ]);
  }
  res.json();
};

exports.createCompany = async (req, res) => {
  const order = req.body;
  if (!order.location || !checkDistace(order.location)) {
    res
      .status(422)
      .json({ message: 'Locatia nu se afla in raza noastra de actiune' });
    return;
  }
  const company = await companyRepository.findOneByUserId(req.userId);
  await orderRepository.create({
    ...order,
    companyId: req.userId,
    companyName: company.name,
    products: [],
    status: orderStatus.PREPARING,
    transportFee: company.transportFee,
  });

  //     courierId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },

  const orders = await orderRepository.findByCompanyId(req.userId);
  res.json(orders);
};

const handleOrders = orders => sortBy(orders, () => 0.5 - Math.random());
