const express = require('express');

const { userRoles } = require('../../constants');

const orderController = require('./orderController');

const {
  authMiddleware,
  asyncMiddleware,
  validatorMiddleware,
  roleMiddleware,
} = require('../../middlewares');

const router = express.Router();

router.get(
  '/order/getCompany',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY]),
  asyncMiddleware(orderController.findByCompanyId),
);

router.get(
  '/order/getCourier/:location',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COURIER]),
  asyncMiddleware(orderController.findByCourierId),
);

router.get(
  '/order/get',
  authMiddleware,
  validatorMiddleware,
  asyncMiddleware(orderController.findByUserId),
);

router.get(
  '/order/getDetail/:orderId',
  authMiddleware,
  validatorMiddleware,
  asyncMiddleware(orderController.getDetails),
);

router.put(
  '/order/nextStatus/:orderId',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY, userRoles.COURIER]),
  asyncMiddleware(orderController.nextStatus),
);

router.post(
  '/order',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.USER]),
  asyncMiddleware(orderController.create),
);

router.post(
  '/order/create',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY]),
  asyncMiddleware(orderController.createCompany),
);

module.exports = router;
