const express = require('express');

const { asyncMiddleware, validatorMiddleware } = require('../../middlewares');

const versionController = require('./versionController');

const router = express.Router();

router.get(
  '/version',
  validatorMiddleware,
  asyncMiddleware(versionController.getVersion),
);
module.exports = router;
