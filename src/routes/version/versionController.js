const { version } = require('../../../about.json');

exports.getVersion = (req, res) => {
  res.json(version);
};
