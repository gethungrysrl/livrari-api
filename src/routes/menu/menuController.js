const { menuRepository, companyRepository } = require('../../repositories');

exports.findOneByUserId = async (req, res) => {
  const { userId } = req;
  const menu = await menuRepository.findOneByUserId(userId);
  res.json(menu);
};

exports.findOneByCompanyId = async (req, res) => {
  const { companyId } = req.params;
  const menu = await menuRepository.findOneByCompanyId(companyId);
  res.json(menu);
};

exports.addCategory = async (req, res) => {
  const { companyId } = req.params;
  const { category } = req.body;
  const menu = await menuRepository.addCategory(companyId, category);
  res.json(menu);
};

exports.editCategory = async (req, res) => {
  const { companyId } = req.params;
  const { category } = req.body;
  const menu = await menuRepository.editCategory(companyId, category);
  res.json(menu);
};

exports.deleteCategory = async (req, res) => {
  const { userId } = req;
  const company = await companyRepository.findOneByUserId(userId);
  const { categoryId } = req.params;
  const menu = await menuRepository.deleteCategory(company.id, categoryId);
  res.json(menu);
};

exports.addProduct = async (req, res) => {
  const { categoryId } = req.params;
  const { companyId, product } = req.body;
  const menu = await menuRepository.addProduct(companyId, categoryId, product);
  res.json(menu);
};

exports.editProduct = async (req, res) => {
  const { categoryId } = req.params;
  const { companyId, product } = req.body;
  const menu = await menuRepository.editProduct(companyId, categoryId, product);
  res.json(menu);
};

exports.deleteProduct = async (req, res) => {
  const { productId } = req.params;
  const { userId } = req;
  const menu = await menuRepository.deleteProduct(userId, productId);
  res.json(menu);
};
