const express = require('express');

const {
  authMiddleware,
  asyncMiddleware,
  validatorMiddleware,
  roleMiddleware,
} = require('../../middlewares');

const { userRoles } = require('../../constants');

const menuController = require('./menuController');

const router = express.Router();

router.get(
  '/menu/get',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY]),
  asyncMiddleware(menuController.findOneByUserId),
);

router.get(
  '/menu/get/:companyId',
  authMiddleware,
  validatorMiddleware,
  asyncMiddleware(menuController.findOneByCompanyId),
);

router.put(
  '/menu/addCategory/:companyId',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY]),
  asyncMiddleware(menuController.addCategory),
);

router.put(
  '/menu/editCategory/:companyId',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY]),
  asyncMiddleware(menuController.editCategory),
);

router.delete(
  '/menu/deleteCategory/:categoryId',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY]),
  asyncMiddleware(menuController.deleteCategory),
);

router.put(
  '/menu/addProduct/:categoryId',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY]),
  asyncMiddleware(menuController.addProduct),
);

router.put(
  '/menu/editProduct/:categoryId',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY]),
  asyncMiddleware(menuController.editProduct),
);

router.delete(
  '/menu/deleteProduct/:productId',
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.COMPANY]),
  asyncMiddleware(menuController.deleteProduct),
);

module.exports = router;
