const { body, query, param } = require('../../validator');
const { userRepository } = require('../../repositories');

const phoneRule = (from = body) => from('phone').isPhoneNumber();
const passwordRule = body('password').isPassword();
const googleAccessTokenRule = body('googleAccessToken').isText();
const facebookAccessTokenRule = body('facebookAccessToken').isText();
const refreshTokenRule = body('refreshToken').isText();
const oldPasswordRule = body('oldPassword').isText();
const newPasswordRule = body('newPassword').isPassword();
const passwordResetTokenRule = query('passwordResetToken').isText();
const confirmationCodeRule = param('code').isText();

const uniquePhoneRule = body('phone')
  .isPhoneNumber()
  .custom(async (phone, { req }) => {
    const user = await userRepository.findByPhone(phone);
    return user && user.id !== String(req.userId)
      ? Promise.reject(new Error('Numar de telefon deja folosit'))
      : Promise.resolve(true);
  });

module.exports = {
  signUp: [uniquePhoneRule, passwordRule],
  login: [phoneRule(), passwordRule],
  loginWithGoogle: [googleAccessTokenRule],
  loginWithFacebook: [facebookAccessTokenRule],
  refreshAuthToken: [refreshTokenRule],
  isPhoneRegistered: [],
  updatePassword: [oldPasswordRule, newPasswordRule],
  createPasswordResetToken: [phoneRule()],
  isValidPasswordResetToken: [passwordResetTokenRule],
  resetPassword: [passwordRule],
  confirmationCode: [confirmationCodeRule],
};
