const { eventEmitter } = require('../../services');
const { eventTypes } = require('../../constants');
const { userRepository, userTokenRepository } = require('../../repositories');

exports.signUp = async (req, res) => {
  const user = await userRepository.create(req.body);
  const authToken = await userTokenRepository.createAuthToken(user.id);
  const refreshToken = await userTokenRepository.createRefreshToken(user.id);

  // eventEmitter.emit(eventTypes.SIGN_UP, user);

  res.set('AuthToken', authToken.token);
  res.set('RefreshToken', refreshToken.token);
  res.json(user);
};

exports.login = async (req, res) => {
  const { phone, password } = req.body;
  const user = await userRepository.findByPhone(phone);
  if (user && !(await userRepository.verifyPassword(user.id, password))) {
    res.status(401).json({ message: 'Invalid credentials.' });
    return;
  }

  const authToken = await userTokenRepository.createAuthToken(user.id);
  const refreshToken = await userTokenRepository.createRefreshToken(user.id);

  res.set('AuthToken', authToken.token);
  res.set('RefreshToken', refreshToken.token);
  res.json(user);
};

exports.refreshAuthToken = async (req, res) => {
  const refreshToken = await userTokenRepository.findRefreshToken(
    req.body.refreshToken,
  );

  if (!refreshToken) {
    res.status(401).json({ message: 'Invalid refresh token.' });
    return;
  }
  const user = await userRepository.findById(refreshToken.userId);
  let authToken = await userTokenRepository.findByUserId(user.id);
  if (!authToken) {
    authToken = await userTokenRepository.createAuthToken(user.id);
  }

  res.set('AuthToken', authToken.token);
  res.set('RefreshToken', refreshToken.token);
  res.json(user);
};

exports.updatePassword = async (req, res) => {
  const { userId } = req;
  const isValidPassword = await userRepository.verifyPassword(
    userId,
    req.body.oldPassword,
  );

  if (!isValidPassword) {
    res.status(403).json({ message: 'Invalid old password.' });
    return;
  }

  await userRepository.findByIdAndUpdatePassword(userId, req.body.newPassword);
  res.json();
};

exports.isPhoneRegistered = async (req, res) => {
  const user = await userRepository.findByPhone(req.params.phone);
  res.json({ isRegistered: !!user });
};

exports.isActive = async (req, res) => {
  const user = await userRepository.findById(req.userId);
  res.json(user.active);
};

exports.createPasswordResetToken = async (req, res) => {
  const user = await userRepository.findByEmail(req.body.email);

  if (!user) {
    res
      .status(422)
      .json({ message: 'Could not find an account with this email.' });

    return;
  }

  const passwordResetToken = await userTokenRepository.createPasswordResetToken(
    user,
  );

  eventEmitter.emit(
    eventTypes.PASSWORD_RESET_TOKEN_CREATED,
    passwordResetToken,
  );

  res.json();
};

exports.isValidPasswordResetToken = async (req, res) => {
  const passwordResetToken = await userTokenRepository.findPasswordResetToken(
    req.query.passwordResetToken,
  );
  res.json({ isValid: !!passwordResetToken });
};

exports.resetPassword = async (req, res) => {
  const { code, phone, password } = req.body;
  const user = await userRepository.changePassword(code, phone, password);
  if (!user) {
    res
      .status(422)
      .json({ message: 'A aparut o eroare la setarea noii parole' });
    return;
  }
  res.json(user);
};

exports.logout = async (req, res) => {
  await userTokenRepository.deleteAllAuthAndRefreshTokensForUser(req.userId);
  res.json('gata');
};

exports.findAllUsers = async (req, res) => {
  const filter = req.query.filter || '';
  const limit = !Number.isNaN(parseInt(req.query.limit, 10))
    ? parseInt(req.query.limit, 10)
    : 5;
  const page = !Number.isNaN(parseInt(req.query.page, 10))
    ? parseInt(req.query.page, 10)
    : 1;
  const withoutroles = req.query.withoutroles || 'false';
  const users = await userRepository.findAll(filter, page, limit, withoutroles);
  res.json(users);
};

exports.confirmCode = async (req, res) => {
  const {
    userId,
    params: { code },
  } = req;
  const user = await userRepository.findById(userId);
  if (user.confirmationCode !== code) {
    res.status(422).json({ message: 'Cod incorect!' });
    return;
  }
  await userRepository.active(userId);
  res.json();
};

exports.createPasswordResetCode = async (req, res) => {
  await userRepository.createPasswordResetCode(req.params.phone);
  res.json();
};

exports.findPasswordResetCode = async (req, res) => {
  const user = await userRepository.isPasswordResetCode(
    req.params.code,
    req.params.phone,
  );
  res.json(!!user);
};

exports.createPushToken = async (req, res) => {
  await userTokenRepository.createPushToken(req.userId, req.params.token);
  res.json({});
};
