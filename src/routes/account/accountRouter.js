const express = require('express');

const {
  authMiddleware,
  asyncMiddleware,
  validatorMiddleware,
  roleMiddleware,
} = require('../../middlewares');
const accountRules = require('./accountRules');
const accountController = require('./accountController');
const { userRoles } = require('../../constants');

const router = express.Router();

router.post(
  '/account/sign-up',
  accountRules.signUp,
  validatorMiddleware,
  asyncMiddleware(accountController.signUp),
);

router.post(
  '/account/login',
  accountRules.login,
  validatorMiddleware,
  asyncMiddleware(accountController.login),
);

router.post(
  '/account/refresh-auth-token',
  accountRules.refreshAuthToken,
  validatorMiddleware,
  asyncMiddleware(accountController.refreshAuthToken),
);

router.post(
  '/account/password',
  authMiddleware,
  accountRules.updatePassword,
  validatorMiddleware,
  asyncMiddleware(accountController.updatePassword),
);

router.get(
  '/account/is-phone-registered/:phone',
  accountRules.isPhoneRegistered,
  validatorMiddleware,
  asyncMiddleware(accountController.isPhoneRegistered),
);

router.get(
  '/account/is-active',
  validatorMiddleware,
  authMiddleware,
  asyncMiddleware(accountController.isActive),
);

router.put(
  '/account/confirm-code/:code',
  accountRules.confirmationCode,
  authMiddleware,
  validatorMiddleware,
  roleMiddleware([userRoles.USER]),
  asyncMiddleware(accountController.confirmCode),
);

router.post(
  '/account/create-password-reset-token',
  accountRules.createPasswordResetToken,
  validatorMiddleware,
  asyncMiddleware(accountController.createPasswordResetToken),
);

router.get(
  '/account/is-valid-password-reset-token',
  accountRules.isValidPasswordResetToken,
  validatorMiddleware,
  asyncMiddleware(accountController.isValidPasswordResetToken),
);

router.post(
  '/account/reset-password',
  accountRules.resetPassword,
  validatorMiddleware,
  asyncMiddleware(accountController.resetPassword),
);

router.post(
  '/account/logout',
  authMiddleware,
  asyncMiddleware(accountController.logout),
);

router.get(
  '/account/all-users',
  authMiddleware,
  asyncMiddleware(accountController.findAllUsers),
);

router.post(
  '/account/create-password-code/:phone',
  asyncMiddleware(accountController.createPasswordResetCode),
);

router.get(
  '/account/is-password-code/:code/:phone',
  asyncMiddleware(accountController.findPasswordResetCode),
);

router.post(
  '/account/pushToken/:token',
  authMiddleware,
  asyncMiddleware(accountController.createPushToken),
);

module.exports = router;
