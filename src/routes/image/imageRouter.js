const express = require('express');
const Multer = require('multer');
const {
  asyncMiddleware,
  authMiddleware,
  googleCloudStorageMiddleware,
} = require('../../middlewares');

const imageController = require('./imageController');

const multer = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: 10 * 1024 * 1024, // Maximum file size is 10MB
  },
});

const router = express.Router();
router.post(
  '/image/upload',
  authMiddleware,
  multer.single('image'),
  googleCloudStorageMiddleware.sendUploadToGCS,
  asyncMiddleware(imageController.uploadeImage),
);
module.exports = router;
