const { indexOf, forEach } = require('lodash');
const User = require('../models/user');

const fetchRoles = async userId => {
  const user = await User.findById(userId);
  return user.roles;
};

const roleMiddleware = (acceptedRoles = []) => async (req, res, next) => {
  req.roles = await fetchRoles(req.userId);
  let unauthorized = true;

  forEach(acceptedRoles, role => {
    if (indexOf(req.roles, role) > -1) {
      unauthorized = false;
    }
  });

  if (unauthorized) {
    return res.status(404).json({ errors: { permission: 'Unauthorized' } });
  }

  next();
};

module.exports = roleMiddleware;
