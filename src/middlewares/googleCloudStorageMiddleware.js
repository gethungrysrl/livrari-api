const gcsHelpers = require('../services/googleCloudStorage');

const { storage } = gcsHelpers;

const { BUCKET_NAME } = process.env;

exports.sendUploadToGCS = async (req, res, next) => {
  if (!req.file) {
    return next();
  }

  const bucket = storage.bucket(BUCKET_NAME);

  const gcsFileName = req.file.originalname;
  const file = bucket.file(gcsFileName);

  const stream = file.createWriteStream({
    resumable: false,
    gzip: true,
    metadata: {
      contentType: req.file.mimetype,
    },
  });

  stream.on('error', err => {
    req.file.cloudStorageError = err;
    next(err);
  });

  stream.on('finish', () => {
    req.file.cloudStorageObject = gcsFileName;

    return file.makePublic().then(() => {
      req.file.gcsUrl = gcsHelpers.getPublicUrl(BUCKET_NAME, gcsFileName);
      next();
    });
  });

  stream.end(req.file.buffer);
};
