require('dotenv').config();
const debug = require('debug')('app');
const autoIncrement = require('mongoose-auto-increment');
const http = require('http');
const socketIO = require('socket.io');

const { database } = require('./services');
const { databaseConfig } = require('./config');
const sockets = require('./sockets');

database.connect(databaseConfig.url, databaseConfig.options);
autoIncrement.initialize(database.connection);

const app = require('./app');

const server = http.createServer(app);
const io = socketIO.listen(server);
sockets.startSockets(io);

process.on('SIGINT', () => {
  database.connection.close(() => {
    process.exit(0);
  });
});

server.listen(process.env.SERVER_PORT, () => {
  debug(`Server listening on http://localhost:${process.env.SERVER_PORT}`);
});
