const { omit } = require('lodash');

const { Company } = require('../models');

const { companyStatus } = require('../constants');

exports.create = values =>
  Company.create({ ...values, status: companyStatus.INACTIVE }, {});

exports.findOneByUserId = userId => Company.findOne({ userId });
exports.findById = id => Company.findById(id);

exports.findOneByUserIdAndUpdate = (userId, company) =>
  Company.findOneAndUpdate(
    { userId },
    { ...omit(company, 'userId', 'id', 'status') },
    { returnOriginal: false },
  );

exports.findActive = () =>
  Company.find({ active: true, status: companyStatus.ACTIVE });
