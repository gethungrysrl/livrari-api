const { omit } = require('lodash');

const { Profile } = require('../models');

exports.findByUserId = userId => Profile.findOne({ userId });

exports.findById = profileId => Profile.findById(profileId);

exports.findOneByUserIdAndUpdate = (userId, profile) =>
  Profile.findOneAndUpdate(
    { userId },
    { ...omit(profile, 'userId', 'id') },
    { returnOriginal: false },
  );

exports.isWorkingHandler = async userId => {
  const profile = await Profile.findOne({ userId });
  const newProfile = await Profile.findOneAndUpdate(
    { userId },
    { isWorking: !profile.isWorking },
    { returnOriginal: false },
  );
  return newProfile;
};
