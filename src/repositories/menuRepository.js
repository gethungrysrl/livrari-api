const { omit } = require('lodash');

const { Menu, Company } = require('../models');

exports.findOneByUserId = async userId => {
  const company = await Company.findOne({ userId });
  let menu = await Menu.findOne({ companyId: company.id });
  if (!menu) {
    menu = Menu.create({ companyId: company.id }, {});
  }
  return menu;
};

exports.findOneByCompanyId = companyId => Menu.findOne({ companyId });

exports.addCategory = (companyId, category) =>
  Menu.findOneAndUpdate(
    { companyId },
    { $push: { categories: category } },
    { returnOriginal: false },
  );

exports.editCategory = (companyId, category) =>
  Menu.findOneAndUpdate(
    { companyId, 'categories._id': category._id },
    {
      $set: {
        'categories.$.name': category.name,
        'categories.$.order': category.order,
      },
    },
    { returnOriginal: false },
  );

exports.deleteCategory = (companyId, categoryId) =>
  Menu.findOneAndUpdate(
    { companyId },
    {
      $pull: {
        categories: { _id: categoryId },
      },
    },
    { returnOriginal: false },
  );

exports.addProduct = (companyId, categoryId, product) =>
  Menu.findOneAndUpdate(
    { companyId, 'categories._id': categoryId },
    {
      $push: {
        'categories.$.products': { ...product, companyId },
      },
    },
    { returnOriginal: false, new: true },
  );

exports.editProduct = async (companyId, categoryId, product) => {
  await Menu.findOneAndUpdate(
    { companyId, 'categories._id': categoryId },
    {
      $pull: {
        'categories.$.products': { _id: product._id },
      },
    },
    { returnOriginal: false, new: true },
  );
  return Menu.findOneAndUpdate(
    { companyId, 'categories._id': categoryId },
    {
      $push: {
        'categories.$.products': product,
      },
    },
    { returnOriginal: false, new: true },
  );
};

exports.deleteProduct = async (userId, productId) => {
  const company = await Company.findOne({ userId });
  const companyId = company.id;
  const menu = await Menu.findOne({
    companyId,
    categories: {
      $elemMatch: { products: { $elemMatch: { _id: productId } } },
    },
  });
  const categoryId = menu.categories[0].id;
  return Menu.findOneAndUpdate(
    { companyId, 'categories._id': categoryId },
    {
      $pull: {
        'categories.$.products': { _id: productId },
      },
    },
    { returnOriginal: false, new: true },
  );
};
