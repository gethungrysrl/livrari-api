const { map, filter } = require('lodash');
const { Order, Profile, Company } = require('../models');

const { orderStatus } = require('../constants');
const userTokenRepository = require('./userTokenRepository');
const sendNotifications = require('../services/sendNotifications');

exports.create = async order => Order.create([{ ...order }]);
exports.findByCompanyId = companyId => Order.find({ companyId });
exports.findByCourierId = courierId =>
  Order.find({ courierId }).populate({ path: 'companyId', select: 'name' });
exports.findByUserId = userId => Order.find({ userId });

exports.detailsOrder = async orderId => {
  const order = await Order.findById(orderId);
  const company = await Company.findOne({ userId: order.companyId });
  const user = await Profile.findOne({ userId: order.userId });
  const courier = await Profile.findOne({ userId: order.courierId });
  return { order, company, user, courier };
};

exports.nextStatus = async orderId => {
  const order = await Order.findById(orderId);
  const company = await Company.findOne({ userId: order.companyId });
  let courierId;
  switch (order.status) {
    case orderStatus.AWAIT: {
      if (company.withDelivery) {
        // eslint-disable-next-line no-use-before-define
        courierId = await handlerCouriers(order);
      }
      await Order.findByIdAndUpdate(orderId, {
        status: orderStatus.PREPARING,
        courierId,
      });
      const userPushToken = await userTokenRepository.findPushToken(
        order.userId,
      );
      sendNotifications.sendNotifications('newOrderStatus', [
        {
          userId: order.userId,
          pushToken: userPushToken.token,
          title: 'Comanda ta se prepara',
          body: 'Intra acum sa vezi noul stadiu a comenzii tale.',
        },
      ]);
      break;
    }
    case orderStatus.PREPARING: {
      await Order.findByIdAndUpdate(orderId, {
        status: orderStatus.WAITING,
      });
      const courierPushToken = await userTokenRepository.findPushToken(
        order.courierId,
      );
      const userPushToken = await userTokenRepository.findPushToken(
        order.userId,
      );
      sendNotifications.sendNotifications('newOrderStatus', [
        {
          userId: order.courierId,
          pushToken: courierPushToken.token,
          title: 'Comanda te asteapta',
          body: 'Intra acum sa vezi detaliile comenzii.',
        },
        {
          userId: order.userId,
          pushToken: userPushToken.token,
          title: 'Comanda ta se livreaza',
          body: 'Intra acum sa vezi noul stadiu a comenzii tale.',
        },
      ]);
      break;
    }
    case orderStatus.WAITING: {
      await Order.findByIdAndUpdate(orderId, {
        status: orderStatus.DELIVERING,
      });
      break;
    }

    case orderStatus.DELIVERING: {
      await Order.findByIdAndUpdate(orderId, {
        status: orderStatus.FINISHED,
      });
      const companyPushToken = await userTokenRepository.findPushToken(
        order.companyId,
      );
      const userPushToken = await userTokenRepository.findPushToken(
        order.userId,
      );
      sendNotifications.sendNotifications('finishOrder', [
        {
          userId: order.companyId,
          pushToken: companyPushToken.token,
          title: 'Comanda finalizata',
          body: 'Felicitari! Ai inca o omanda finalizata cu success.',
        },
        {
          userId: order.userId,
          pushToken: userPushToken.token,
          title: 'Comanda finalizata',
          body:
            'Multumim pentru incredere! Iti asteptam noile comezi cu placere.',
        },
      ]);
      break;
    }
    default:
      break;
  }
  return order;
};

exports.asignOrders = async courierId => {
  const orders = await Order.find({
    courierId: undefined,
    status: { $in: [orderStatus.PREPARING, orderStatus.DELIVERING] },
  });
  const asignedOrders = filter(
    await Promise.all(
      map(orders, async order => {
        const company = await Company.findOne({ userId: order.companyId });
        return company.withDelivery
          ? // eslint-disable-next-line no-underscore-dangle
            Order.findByIdAndUpdate(order._id, { courierId })
          : undefined;
      }),
    ),
    order => order,
  );
  if (asignedOrders.length) {
    const userPushToken = await userTokenRepository.findPushToken(courierId);
    sendNotifications.sendNotifications('newOrder', [
      {
        userId: courierId,
        pushToken: userPushToken.token,
        title: 'Comanda noua',
        body: 'Intra acum sa vezi detaliile comenzii.',
      },
    ]);
  }
};

const handlerCouriers = async () => {
  const workingCouriers = await Profile.find({ isWorking: true });
  if (!!workingCouriers && workingCouriers.length) {
    const courierId = workingCouriers[0].userId;
    const userPushToken = await userTokenRepository.findPushToken(courierId);
    sendNotifications.sendNotifications('newOrder', [
      {
        userId: courierId,
        pushToken: userPushToken.token,
        title: 'Comanda noua',
        body: 'Intra acum sa vezi detaliile comenzii.',
      },
    ]);
    return courierId;
  }
  return undefined;
};
