const bcrypt = require('bcryptjs');
const { omit } = require('lodash');
const { accountConfig } = require('../config');
const { User, Profile } = require('../models');
const { database } = require('../services');
const { userRoles } = require('../constants');

exports.create = async values => {
  const session = await database.startSession();
  session.startTransaction();

  try {
    const hashedPassword = await bcrypt.hash(
      values.password,
      accountConfig.bcrypt.salts,
    );

    const inviteLinkToken = Math.random().toString(36).substring(5);

    const user = await User.create(
      [
        {
          phone: values.phone,
          password: hashedPassword,
          confirmationCode: 3333, //String(Math.floor(1000 + Math.random() * 9000)),
          roles: [userRoles.USER],
          inviteLinkToken,
          active: false,
        },
      ],
      { session },
    );

    const userProfile = await Profile.create(
      [
        {
          userId: user[0].id,
          firstName: values.firstName,
          lastName: values.lastName,
          phone: user[0].phone,
          profilePicture: '',
        },
      ],
      { session },
    );

    const newUser = await User.findOneAndUpdate(
      {
        _id: user[0].id,
      },
      { userProfileId: userProfile[0].id },
      { new: true },
    ).session(session);

    session.commitTransaction();

    return newUser;
  } catch (error) {
    session.abortTransaction();
    throw error;
  }
};

exports.validateAccount = async (userId, code) => {
  const user = await User.findById(userId);

  if (user.confirmationCode === code) {
    user.isConfirmed = true;
    user.save();
    return { validationStatus: true };
  }
  return { validationStatus: false };
};

exports.findOrCreateWithGoogleProfile = async profile => {
  const { googleId, email } = profile;

  const userByGoogleId = await User.findOne({ googleId });
  if (userByGoogleId) return userByGoogleId;

  const userByEmail = await User.findOneAndUpdate(
    { email },
    { $set: { googleId } },
    { new: true },
  );
  if (userByEmail) return userByEmail;

  return User.create({ ...profile, password: '' });
};

exports.findOrCreateWithFacebookProfile = async profile => {
  const { facebookId, email } = profile;

  const userByFacebookId = await User.findOne({ facebookId });
  if (userByFacebookId) return userByFacebookId;

  const userByEmail = await User.findOneAndUpdate(
    { email },
    { $set: { facebookId } },
    { new: true },
  );
  if (userByEmail) return userByEmail;

  return User.create({ ...profile, password: '' });
};

exports.findByCredentials = async (email, password) => {
  const user = await User.findOne({ email });
  if (!user) return;

  const isValidPassword = await bcrypt.compare(password, user.password);
  return isValidPassword ? user : undefined;
};

exports.verifyPassword = async (userId, password) => {
  const user = await User.findById(userId);
  return !!user && bcrypt.compare(password, user.password);
};

exports.findByIdAndUpdate = async (userId, values) =>
  User.findByIdAndUpdate(
    userId,
    { $set: omit(values, 'password') },
    { new: true },
  );

exports.findByIdAndUpdatePassword = async (userId, password) => {
  const hashedPassword = await bcrypt.hash(
    password,
    accountConfig.bcrypt.salts,
  );

  return User.findByIdAndUpdate(
    userId,
    { $set: { password: hashedPassword } },
    { new: true },
  );
};

exports.findByInvitationToken = inviteLinkToken =>
  User.findOne({ inviteLinkToken });

exports.findById = userId => User.findById(userId);
exports.findByPhone = phone => User.findOne({ phone });
exports.findAllByIds = userIds => User.find({ _id: { $in: userIds } });

exports.active = userId => User.findByIdAndUpdate(userId, { active: true });

exports.createPasswordResetCode = phone =>
  User.findOneAndUpdate(
    { phone },
    {
      passwordCode: 3333, //String(Math.floor(1000 + Math.random() * 9000))
    },
  );

exports.isPasswordResetCode = (code, phone) =>
  User.findOne({ phone, passwordCode: code });

exports.changePassword = async (code, phone, password) => {
  try {
    const hashedPassword = await bcrypt.hash(
      password,
      accountConfig.bcrypt.salts,
    );
    return User.findOneAndUpdate(
      { phone, passwordCode: code },
      { $set: { password: hashedPassword, passwordCode: '' } },
    );
  } catch (e) {
    return null;
  }
};
