const ms = require('ms');
const uniqString = require('uniq-string');
const { accountConfig } = require('../config');
const { UserToken } = require('../models');

exports.findByUserId = userId =>
  UserToken.findOne({ userId, type: UserToken.types.AUTH_TOKEN });

const createUserToken = (userId, type, length, expiresIn) =>
  UserToken.create({
    userId,
    type,
    token: uniqString(length),
    expiresAt: new Date(Date.now() + ms(expiresIn)),
  });

exports.createAuthToken = userId =>
  createUserToken(
    userId,
    UserToken.types.AUTH_TOKEN,
    accountConfig.tokens.auth.length,
    accountConfig.tokens.auth.expiresIn,
  );

exports.createRefreshToken = userId =>
  createUserToken(
    userId,
    UserToken.types.REFRESH_TOKEN,
    accountConfig.tokens.refresh.length,
    accountConfig.tokens.refresh.expiresIn,
  );

exports.createPasswordResetToken = userId =>
  createUserToken(
    userId,
    UserToken.types.PASSWORD_RESET_TOKEN,
    accountConfig.tokens.passwordReset.length,
    accountConfig.tokens.passwordReset.expiresIn,
  );

exports.createPushToken = async (userId, token) => {
  const userToken = await UserToken.findOne({
    userId,
    type: UserToken.types.PUSH_TOKEN,
    token,
  });
  if (!userToken) {
    UserToken.create({
      userId,
      type: UserToken.types.PUSH_TOKEN,
      token,
      expiresAt: new Date(Date.now() + ms(accountConfig.tokens.push.expiresIn)),
    });
  }
};

exports.findAuthToken = token =>
  UserToken.findOne({ token, type: UserToken.types.AUTH_TOKEN });

exports.findRefreshToken = token =>
  UserToken.findOne({ token, type: UserToken.types.REFRESH_TOKEN });

exports.findPasswordResetToken = token =>
  UserToken.findOne({
    token,
    type: UserToken.types.PASSWORD_RESET_TOKEN,
  });

exports.findPushToken = userId =>
  UserToken.findOne({ userId, type: UserToken.types.PUSH_TOKEN });

exports.deleteAllAuthAndRefreshTokensForUser = async userId =>
  UserToken.deleteMany({
    userId,
    type: {
      $in: [UserToken.types.AUTH_TOKEN, UserToken.types.REFRESH_TOKEN],
    },
  });

exports.deleteAllPasswordResetTokensForUser = userId =>
  UserToken.deleteMany({
    userId,
    type: UserToken.types.PASSWORD_RESET_TOKEN,
  });
