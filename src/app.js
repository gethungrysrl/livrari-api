require('dotenv').load();
const express = require('express');
const passport = require('passport');
const sentry = require('@sentry/node');

const { errorHandlerMiddleware } = require('./middlewares');
const { accountConfig } = require('./config');

const { authStrategy } = require('./middlewares');
const accountRouter = require('./routes/account/accountRouter');
const profileRouter = require('./routes/profile/profileRouter');
const companyRouter = require('./routes/company/companyRouter');
const imageRouter = require('./routes/image/imageRouter');
const menuRouter = require('./routes/menu/menuRouter');
const orderRouter = require('./routes/order/orderRouter');
const versionRouter = require('./routes/version/versionRouter');

const bootstrap = require('./bootstrap');

const app = express();
bootstrap();

// sentry.init({ dsn: process.env.SENTRY_DSN });
passport.use(authStrategy);

app.use(sentry.Handlers.requestHandler());
app.use(express.json());

app.use(passport.initialize(accountConfig.passport.initialization));
app.use(accountRouter);
app.use(profileRouter);
app.use(companyRouter);
app.use(imageRouter);
app.use(menuRouter);
app.use(orderRouter);
app.use(versionRouter);

app.use(sentry.Handlers.errorHandler());
app.use(errorHandlerMiddleware);

module.exports = app;
