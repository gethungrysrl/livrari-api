const path = require('path');
const { sendEmail } = require('../services');
const { appConfig } = require('../config');

exports.send = (user, passwordResetToken) =>
  sendEmail({
    to: user.email,
    subject: `[${appConfig.name}] Resetare parola`,
    templatePath: path.resolve(
      __dirname,
      '../templates/resetPasswordEmail.hbs',
    ),
    context: {
      user,
      passwordResetToken,
      appConfig,
    },
  });
