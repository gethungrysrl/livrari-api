const path = require('path');
const { sendEmail } = require('../services');
const { appConfig } = require('../config');

exports.send = user =>
  sendEmail({
    to: user.email,
    subject: `[${appConfig.name}] Confirmare de inregistrare`,
    templatePath: path.resolve(
      __dirname,
      '../templates/signUpConfirmationEmail.hbs',
    ),
    context: {
      user,
      appConfig,
    },
  });
