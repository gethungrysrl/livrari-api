const { signUpConfirmationEmail } = require("../emails");

const sendSignUpConfirmationEmail = async user => {
  return signUpConfirmationEmail.send(user);
};

module.exports = sendSignUpConfirmationEmail;
