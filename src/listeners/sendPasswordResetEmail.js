const { User } = require('../models');
const { passwordResetEmail } = require('../emails');

const sendPasswordResetEmail = async passwordResetToken => {
  const { userId } = passwordResetToken;
  const user = await User.findById(userId);

  return passwordResetEmail.send(user, passwordResetToken);
};

module.exports = sendPasswordResetEmail;
