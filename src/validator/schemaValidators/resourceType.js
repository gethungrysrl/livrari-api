const joi = require('joi');
const { values } = require('lodash');
const { resourceTypes } = require('../constants');

const validResourceTypes = values(resourceTypes);

const resourceType = {
  name: 'resourceType',
  language: {
    base: 'Must be a valid resource type',
  },
  base: joi.string(),
  pre(value, state, options) {
    if (validResourceTypes.indexOf(value) === -1) {
      return this.createError('resourceType.base', { value }, state, options);
    }

    return value;
  },
};

module.exports = resourceType;
