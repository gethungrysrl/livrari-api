const joi = require('joi');
const { isMongoId } = require('validator');

const mongoId = {
  name: 'mongoId',
  language: {
    base: 'Must be a valid Mongo id',
  },
  base: joi.string(),
  pre(value, state, options) {
    if (!isMongoId(value) && value.length !== 21) {
      return this.createError('mongoId.base', { value }, state, options);
    }

    return value;
  },
};

module.exports = mongoId;
