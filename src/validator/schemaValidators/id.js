const joi = require('joi');
const { isMongoId } = require('validator');
const { isNanoId } = require('../helpers');

const id = {
  name: 'id',
  language: {
    base: 'Must be a valid Mongo or Nano id',
  },
  base: joi.string(),
  pre(value, state, options) {
    if (!isMongoId(value) && !isNanoId(value)) {
      return this.createError('id.base', { value }, state, options);
    }

    return value;
  },
};

module.exports = id;
