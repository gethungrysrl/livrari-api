const joi = require('joi');
const { values } = require('lodash');
const { permissions } = require('../constants');

const validPermissions = values(permissions);

const permission = {
  name: 'permission',
  language: {
    base: 'Must be a valid permission',
  },
  base: joi.string(),
  pre(value, state, options) {
    if (validPermissions.indexOf(value) === -1) {
      return this.createError('permission.base', { value }, state, options);
    }

    return value;
  },
};

module.exports = permission;
