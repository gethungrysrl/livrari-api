const joi = require('joi');
const { isNanoId } = require('../helpers');

const nanoId = {
  name: 'nanoId',
  language: {
    base: 'Must be a valid Nano id',
  },
  base: joi.string(),
  pre(value, state, options) {
    if (!isNanoId(value)) {
      return this.createError('nanoId.base', { value }, state, options);
    }

    return value;
  },
};

module.exports = nanoId;
