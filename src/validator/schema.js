const joi = require('joi');
const {
  id,
  mongoId,
  nanoId,
  resourceType,
  permission,
} = require('./schemaValidators');

const schema = joi
  .extend(id)
  .extend(mongoId)
  .extend(nanoId)
  .extend(resourceType)
  .extend(permission);

module.exports = schema;
