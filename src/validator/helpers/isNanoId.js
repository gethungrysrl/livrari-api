const { isString } = require('lodash');

const isNanoId = value => isString(value) && value.length === 21;

module.exports = isNanoId;
