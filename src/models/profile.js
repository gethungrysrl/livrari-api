const { omit } = require('lodash');
const mongoose = require('mongoose');
const { database } = require('../services');

const { Schema } = database;

const toObject = (doc, profile) => omit(profile);

const ProfileSchema = new Schema(
  {
    firstName: String,
    lastName: String,
    imageUrl: String,
    phone: String,
    isWorking: { type: Boolean, default: false },
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
  },
  {
    timestamps: true,
    toObject: { transform: toObject },
    toJSON: { transform: toObject },
  },
);

const Profile = database.model('profile', ProfileSchema);

module.exports = Profile;
