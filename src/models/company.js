const { omit } = require('lodash');
const mongoose = require('mongoose');

const { database } = require('../services');

const { Schema } = database;

const toObject = (doc, company) => omit(company);

const CompanySchema = new Schema(
  {
    name: String,
    status: String,
    description: String,
    email: String,
    imageUrl: String,
    phone: String,
    location: Object,
    openTime: String,
    closeTime: String,
    transportFee: { type: Number, default: 0 },
    withDelivery: { type: Boolean, default: true },
    active: { type: Boolean, default: true },
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    schedule: Array,
    deliverySchedule: Array,
  },
  {
    timestamps: true,
    toObject: { transform: toObject },
    toJSON: { transform: toObject },
  },
);

const Company = database.model('company', CompanySchema);

module.exports = Company;
