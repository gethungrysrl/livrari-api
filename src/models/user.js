const { omit } = require('lodash');
const { database } = require('../services');
const { userRoles } = require('../constants');

const { Schema } = database;

const toObject = (doc, user) => omit(user, 'password');

const UserSchema = new Schema(
  {
    phone: { type: String, unique: true },
    password: String,
    roles: { type: Array, default: [userRoles.USER] },
    confirmationCode: String,
    active: { type: Boolean, default: false },
    passwordCode: String,
  },
  {
    timestamps: true,
    toObject: { transform: toObject },
    toJSON: { transform: toObject },
  },
);

const User = database.model('user', UserSchema);

module.exports = User;
