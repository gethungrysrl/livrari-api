const { omit } = require('lodash');
const mongoose = require('mongoose');

const { database } = require('../services');

const { Schema } = database;

const toObject = (doc, menu) => omit(menu);

exports.productSchema = new Schema(
  {
    name: String,
    imageUrl: String,
    description: String,
    price: Number,
    quantity: Number,
    quantityType: String,
    typeQuantity: String,
    lenght: { type: Number, default: 0 },
    companyId: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
  },
  {
    timestamps: true,
    toObject: { transform: toObject },
    toJSON: { transform: toObject },
  },
);
