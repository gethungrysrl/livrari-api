const { omit } = require('lodash');
const mongoose = require('mongoose');
const { autoIncrement } = require('mongoose-plugin-autoinc');

const { database } = require('../services');
const { productSchema } = require('./product');

const { orderStatus } = require('../constants');

const { Schema } = database;

const toObject = (doc, order) => omit(order);

const OrderSchema = new Schema(
  {
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    courierId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    companyId: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    companyName: { type: String, default: 'name' },
    products: [productSchema],
    status: {
      type: String,
      default: orderStatus.AWAIT,
      enum: [
        orderStatus.AWAIT,
        orderStatus.PREPARING,
        orderStatus.WAITING,
        orderStatus.DELIVERING,
        orderStatus.FINISHED,
      ],
    },
    index: { type: Number },
    total: { type: Number, default: 0 },
    transportFee: { type: Number, default: 0 },
    companyLocation: { type: Object },
    location: { type: Object },
    mentions: { type: String, default: '' },
  },
  {
    timestamps: true,
    toObject: { transform: toObject },
    toJSON: { transform: toObject },
  },
);

OrderSchema.plugin(autoIncrement, {
  model: 'order',
  field: 'index',
  startAt: 1,
  incrementBy: 1,
});

const Order = database.model('order', OrderSchema);

module.exports = Order;
