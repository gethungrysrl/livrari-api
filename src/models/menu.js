const mongoose = require('mongoose');
const { omit } = require('lodash');
const { database } = require('../services');

const { productSchema } = require('./product');

const { Schema } = database;

const toObject = (doc, menu) => omit(menu);

const categorySchema = new Schema(
  {
    name: String,
    order: { type: Number, default: 0 },
    products: [productSchema],
  },
  {
    timestamps: true,
    toObject: { transform: toObject },
    toJSON: { transform: toObject },
  },
);

const MenuSchema = new Schema(
  {
    categories: [categorySchema],
    companyId: { type: mongoose.Schema.Types.ObjectId, ref: 'company' },
  },
  {
    timestamps: true,
    toObject: { transform: toObject },
    toJSON: { transform: toObject },
  },
);

const Menu = database.model('menu', MenuSchema);

module.exports = Menu;
